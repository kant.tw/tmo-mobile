define(['magnifik'], function(magnifik) {
	var magnifik = require(magnifik);

	//pdcontent interaction
	function pdContentUI(options) {
		this.mainSlide = options.mainPicWrap.find('.pdcnt_img');
		this.colorChanger = options.colorChanger;
		this.sizeSelector = options.sizeSelector;
		this.numberChanger = options.numberChanger;
		this.popupContainer = options.popup;
		this.actionBtnWrap = options.btnwrap;
		this.pdnotesElem = options.notesElem;
		this.colorName = options.colorName;
		this.currentQty = 0;
		this.currentColor; 
		this.currentSize; 
		this.currentNumber;		
		
		this.init = function() {

			this.colorEvent();
			this.sizeChangeEvent();
			this.qtyChangEvent();
			this.zoomEvent();
			
		}
			
	}

	pdContentUI.prototype.activeSlider = function(sliderwrap) {
		if (!sliderwrap.hasClass('slick-initialized')) {
			sliderwrap.slick({
				arrows: false,
				dots: true
			});	
		}
	}

	//pdcontent點顏色 換左邊整組圖片&換尺寸數量
	pdContentUI.prototype.colorEvent = function() {
		var self = this;
		this.colorChanger.on('click', function(e) {
			e.preventDefault();
			var colorIdx = $(this).parent('li').index();
			var targetWrap = self.mainSlide.eq(colorIdx);
			
			//改變選顏色區樣式
			self.colorChanger.removeClass('active');
			$(this).addClass('active');


			//改變顏色字
			self.colorName.text($(this).data('colorname'));

			//換左邊整組圖片
			self.mainSlide.removeClass('active');
			targetWrap.addClass('active');
			self.activeSlider(targetWrap);

			//改變popup圖 
			self.popupContainer.find('.pdArrived_img').attr('src', targetWrap.find('li').eq(1).find('img').attr('src'));

			
			//換尺寸數量
			self.sizeSelector.removeClass('active');
			self.sizeSelector.eq(colorIdx).addClass('active');
			self.currentQty = self.sizeSelector.eq(colorIdx).find('li').eq(0).data('qty');
			self.sizeSelector.eq(colorIdx).find('li').eq(0).addClass('active');
			self.updatePopupSize(self.sizeSelector.eq(colorIdx).find('li').eq(0).text());

			//調整數量
			self.setNumberChangerStatus(self.currentQty);

			// self.pdnotesElem.text(self.sizeSelector.eq(colorIdx).find('option:selected').data('notes'));

			//更新值狀態
			self.currentColor = $(this).data('color'); 
			self.currentSize = self.sizeSelector.eq(colorIdx).find('li').text(); 
			self.currentNumber = 1; 
			self.fillHiddenInput(self.currentColor, self.currentSize, self.currentNumber); 

		});
	}


	//切換尺寸
	pdContentUI.prototype.sizeChangeEvent = function() {
		var self = this;
		this.sizeSelector.find('li').on('click', function(e) {
			self.sizeSelector.find('li').removeClass('active');
			$(e.target).addClass('active');
			self.currentSize = $(e.target).text();
			self.currentQty = parseInt($(e.target).data('qty'));
			self.currentNumber = 1;
			
			self.setNumberChangerStatus(self.currentQty);
			
			// self.pdnotesElem.text($(e.target).find('option:selected').data('notes'));
			
			//改變popup裡面的尺寸
			self.updatePopupSize(self.currentSize);

			self.fillHiddenInput(self.currentColor, self.currentSize, self.currentNumber); //new
		});
	}

	pdContentUI.prototype.qtyChangEvent = function() {
		var self = this;
		self.numberChanger.on('change', function(e) {
			self.currentNumber = $(this).val();
			self.fillHiddenInput(self.currentColor, self.currentSize, self.currentNumber);
		});
	}

	pdContentUI.prototype.updatePopupSize = function(currentSize) {
		this.popupContainer.find('.pdArrived_size').text(currentSize);
	}

	pdContentUI.prototype.fillHiddenInput = function(color, size, pdnumber) { 
		$('input[name="pdcolor"]').val(color); 
		$('input[name="pdsize"]').val(size); 
		$('input[name="pdnumber"]').val(pdnumber); 
		console.log('color: ' + $('input[name="pdcolor"]').val() + ',size: ' + $('input[name="pdsize"]').val() + ',number: ' + $('input[name="pdnumber"]').val());
	}

	pdContentUI.prototype.setNumberChangerStatus = function(currQty) { //改變數量選擇狀態
		var self = this;
		var numberchangeHtml = '';
		self.numberChanger.empty();
		if (currQty === 0) { //如果商品數量為0 顯示貨到通知我按鈕
			self.actionBtnWrap.addClass('emptyStock');
			self.numberChanger.parent('.pdcnt_mainDesc_number').hide();
			self.currentNumber = 0;
		} else { //如果商品數量不為0 顯示加入購物車按鈕
			self.actionBtnWrap.removeClass('emptyStock');	
			for (var i = 1; i <= currQty; i++) {
				numberchangeHtml = numberchangeHtml + '<option value="' + i + '">' + i + '</option>';
			}
			self.numberChanger.append(numberchangeHtml);
			self.numberChanger.parent('.pdcnt_mainDesc_number').show();
			self.currentNumber = 1; 
		}
	}

	pdContentUI.prototype.zoomEvent = function() {
		$('.js-magnifik').magnifik();
	}

	return pdContentUI;

})