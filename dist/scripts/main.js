
$(function() {
	$('.js-mfp').magnificPopup();

	$('#header_search').on('click', function(e) {
		if ($(e.target).parents('.header_search_inner').length === 0) {
			$('#header_search_trigger').click();
		}
	});

	$('#mainMenuTrigger').on('click', function(e) {
		if ($(this).hasClass('active')) {
			// disable scroll on content
			$('.content_wrap').addClass('noscroll');
		} else {
			// reable scroll
			$('.content_wrap').removeClass('noscroll');
		}
	});

	$('#contactus').on('click', function(e) { console.log('click');
		if ($(e.target).parents('.contactus_inner').length === 0) { console.log('in ' + $('#contactus_trigger').length);
			$('#contactus_trigger').click();
		}
	});

	$('.js-magnifik').magnifik();

	$(window).on('scroll', function() {
		if ($(document).scrollTop() > 200) {
			$('#goTop').show();
		} else {
			$('#goTop').hide();
		}
	});

	$('body').on('click', '#goTop', function(e) {
		e.preventDefault();
		$('html, body').animate({'scrollTop': 0}, 600);
	});

	
});