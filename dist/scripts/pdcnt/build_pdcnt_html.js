define(function(require) {

    var pdContentUI = require('./pdcontent_ui');

    function buildHTML(imgs) {
        var finalHTML = '';
                                
        for (var colorkey in imgs) {
            var colorsetHTML = '<ul class="pdcnt_img">';

            imgs[colorkey].forEach(function(colorimg) {
                colorsetHTML = colorsetHTML + '<li class=""><a href="' + colorimg + '" class="js-magnifik"><img src="' + colorimg + '" /></a></li>';

            });
            colorsetHTML = colorsetHTML + '</ul>';
            finalHTML = finalHTML + colorsetHTML;
        }

        $('.pdcnt_mainimg').append(finalHTML);
        

        new pdContentUI({
            mainPicWrap: $('.pdcnt_mainimg'), 
            colorChanger: $('.pdcnt_mainDesc_color a'), 
            sizeSelector: $('.js-size'), 
            numberChanger: $('.js-number'), 
            popup: $('#productArrived'), 
            btnwrap: $('.js-actionBtnWrap'), 
            // notesElem: $('.pdcnt_info_note'),
            colorName: $('.pdcnt_mainDesc_colorname')
        }).init();

        $('.pdcnt_mainDesc_color').find('a').eq(0).click();
    }

    return buildHTML;
})
